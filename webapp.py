import socket

class webapp:

    def trocear(self, received):
        recibido = received.decode()
        return recibido.split(' ')[1] # Nos quedamos con la petición hecha al servidor

    # La salida de parse es la entrada de process
    def procesar(self, analyzed):
        http = "200 OK"
        # El analyzed es la petición que obtenemos en el parseado
        html = "<html><body><h1>Hello World! Tu peticion es "+analyzed+"</h1>"\
               + "</body></html>"
        return http, html

    def __init__(self, ip, port):   # Constructor
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.bind((ip, port))

        mySocket.listen(5)
# Ahora escuchamos y luego recibimos la petición:
        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print(address)
            print("HTTP request received:")
            received = recvSocket.recv(2048).decode('utf-8')
            print(received) # Imprimimos la información

            peticion = self.trocear(received)
            http, html = self.procesar(peticion) # var1: http, var2: html

            response = "HTTP/1.1" + http+ "\r\n\r\n" \
            +html + "\r\n"

            recvSocket.send(response.encode('utf-8'))
            recvSocket.close()

if __name__=="__main__":
    webapp = webapp('localhost', 1235)
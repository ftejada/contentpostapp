import webapp

class contentpostApp (webapp.webapp):
    # El recurso está compuesto por clave y valor
    recursos = {'/':'P&aacute;gina principal',
                '/Fer':'P&aacute;gina de Fer',
                '/Prueba':'P&aacute;gina de prueba'} # Si quiero abrir link, mirar cuerpo de los mensajes
    # Si el recurso está dentro del diccionario, lo muestro como recurso principal del html

    def trocear(self, request):
        # CREAMOS FORMULARIO
        recurso = request.split(' ')[1]
        print("Recurso:", recurso)
        body = request.split('=')[-1]
        print("Body:: ", body)
        metodo = request.split(' ')[0]
        print("Metodo:", metodo)

        return recurso, body, metodo
        # La salida de parse es la entrada de process

    def procesar(self, analyzed):
        recurso, body, metodo = analyzed

        formulario = "<p>" + "<form action='' method='POST'><p>" \
                     + "Introduzca la ruta deseada : <input name = 'body'>" \
                     + "<input type='submit' value='Enviar' />" \
                     + "</body></html>"

        if metodo == "GET":
            if recurso in self.recursos.keys():
                http = "200 OK"
                html = '<html><body><meta charset="UTF-8"/>' \
                       + "<p>La ruta " + recurso + " existe y contiene: " + self.recursos[recurso] + "<p>" \
                       + formulario + '<body><html>'
            else:
                http = "404 Not Found"
                html = '<html><body><meta charset="UTF-8"/>' \
                       + "<p>La ruta " + recurso + " ha sido creada<p>" \
                       + formulario + '<body><html>'
        elif metodo == "POST":
            self.recursos[recurso] = body
            # print(self.recursos)
            if recurso in self.recursos.keys():
                http = "200 OK"
                html = '<html><body><meta charset="UTF-8"/>' \
                       + "<p>La ruta " + recurso + " ahora contiene: " + self.recursos[recurso] + "<p>" \
                       + formulario + '<body><html>'
            else: # Metodo PUT
                http = "404 Not Found"
                html = '<html><body><meta charset="UTF-8"/>' \
                       + "<p>La ruta" + recurso + " no existe aunque puedes crear una<p>" \
                       + formulario + '<body><html>'
        else:
            if recurso in self.recursos.keys():
                http = "200 OK"
                html = '<html><body><meta charset="UTF-8"/>' \
                       + "<p>Zona de pruebas "'<body><html>'
            else:
                http = "404 Not Found"
                html = html = "<html><body><img src='https://upload.wikimedia.org/wikipedia/commons/9/9b/404-error-css.png'/></body></html>"
        return http, html

if __name__=="__main__":
    testcontentapp = contentpostApp("localhost", 1234)
